import json
import re
import pandas as pd
import torch
from datasets import Dataset
from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    BitsAndBytesConfig,
    TrainingArguments,
)
from trl import SFTTrainer
from peft import LoraConfig
DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"
MODEL_NAME = "TheBloke/Llama-2-7B-fp16"
# Define your qa_data
qa_data = [
{
"context": "Customer Pardeep Kumar contacts agent Kuldeep for insurance plan details The customer was interested in a 5 lakh policy for himself, his wife Kamini Devi, and two children Agent Kuldeep recommends Aditya Birla Diamond plan, offering comprehensive coverage with no room limitations and coverage for ICU expenses The agent informs the customer about a free health check-up for four members upon policy porting, as well as discounts through physical activities and ambulance benefits Kuldeep mentions hospitals in the customer area and clarifies policy terms The customer inquires about a separate policy for himself, to which the agent suggests a policy from Max Bupa with details on coverage and renewals The customer also mentions an HDFC Bank policy, but the agent advises against it, leaning towards Max Bupa The agent helps the customer fill out the form and explains the process of sending policy details via email The customer provides his renewal date, and the agent guides him on sending emails The customer works a job and receives renewal notices via email The agent explains how to find the relevant email attachment and share it with the agent for processing The customer seeks advice on which policy to purchase and shares personal details such as height, weight, and income The agent informs the customer about waiting periods, critical and seasonal diseases, and exclusions The customer mentions a past operation and inquires about reimbursement, but the agent explains the limitations The agent collects information about the nominee, Kamini Devi, and the customer, Pardeep Kumar The agent arranges a call for OTP verification and advises the customer on the call-back process after filling out the form",
"question": "Who contacts Agent Kuldeep for insurance plan details?",
"answer": "Customer Pardeep Kumar contacts Agent Kuldeep for insurance plan details"
},
{
"context": "Customer Pardeep Kumar contacts agent Kuldeep for insurance plan details The customer was interested in a 5 lakh policy for himself, his wife Kamini Devi, and two children Agent Kuldeep recommends Aditya Birla Diamond plan, offering comprehensive coverage with no room limitations and coverage for ICU expenses The agent informs the customer about a free health check-up for four members upon policy porting, as well as discounts through physical activities and ambulance benefits Kuldeep mentions hospitals in the customer area and clarifies policy terms The customer inquires about a separate policy for himself, to which the agent suggests a policy from Max Bupa with details on coverage and renewals The customer also mentions an HDFC Bank policy, but the agent advises against it, leaning towards Max Bupa The agent helps the customer fill out the form and explains the process of sending policy details via email The customer provides his renewal date, and the agent guides him on sending emails The customer works a job and receives renewal notices via email The agent explains how to find the relevant email attachment and share it with the agent for processing The customer seeks advice on which policy to purchase and shares personal details such as height, weight, and income The agent informs the customer about waiting periods, critical and seasonal diseases, and exclusions The customer mentions a past operation and inquires about reimbursement, but the agent explains the limitations The agent collects information about the nominee, Kamini Devi, and the customer, Pardeep Kumar The agent arranges a call for OTP verification and advises the customer on the call-back process after filling out the form",
"question": "What insurance plan does Agent Kuldeep recommend for the customer?",
"answer": "Agent Kuldeep recommends Aditya Birlas Diamond plan, offering comprehensive coverage with no room limitations and coverage for ICU expenses"
},
{
"context": "Customer Pardeep Kumar contacts agent Kuldeep for insurance plan details The customer was interested in a 5 lakh policy for himself, his wife Kamini Devi, and two children Agent Kuldeep recommends Aditya Birla Diamond plan, offering comprehensive coverage with no room limitations and coverage for ICU expenses The agent informs the customer about a free health check-up for four members upon policy porting, as well as discounts through physical activities and ambulance benefits Kuldeep mentions hospitals in the customer area and clarifies policy terms The customer inquires about a separate policy for himself, to which the agent suggests a policy from Max Bupa with details on coverage and renewals The customer also mentions an HDFC Bank policy, but the agent advises against it, leaning towards Max Bupa The agent helps the customer fill out the form and explains the process of sending policy details via email The customer provides his renewal date, and the agent guides him on sending emails The customer works a job and receives renewal notices via email The agent explains how to find the relevant email attachment and share it with the agent for processing The customer seeks advice on which policy to purchase and shares personal details such as height, weight, and income The agent informs the customer about waiting periods, critical and seasonal diseases, and exclusions The customer mentions a past operation and inquires about reimbursement, but the agent explains the limitations The agent collects information about the nominee, Kamini Devi, and the customer, Pardeep Kumar The agent arranges a call for OTP verification and advises the customer on the call-back process after filling out the form",
"question": "What benefits does the customer learn about when discussing policy options with the agent?",
"answer": "The customer learns about a free health check-up for four members upon policy porting, as well as discounts through physical activities and ambulance benefits"
},
{
"context": "Customer Pardeep Kumar contacts agent Kuldeep for insurance plan details The customer was interested in a 5 lakh policy for himself, his wife Kamini Devi, and two children Agent Kuldeep recommends Aditya Birla Diamond plan, offering comprehensive coverage with no room limitations and coverage for ICU expenses The agent informs the customer about a free health check-up for four members upon policy porting, as well as discounts through physical activities and ambulance benefits Kuldeep mentions hospitals in the customer area and clarifies policy terms The customer inquires about a separate policy for himself, to which the agent suggests a policy from Max Bupa with details on coverage and renewals The customer also mentions an HDFC Bank policy, but the agent advises against it, leaning towards Max Bupa The agent helps the customer fill out the form and explains the process of sending policy details via email The customer provides his renewal date, and the agent guides him on sending emails The customer works a job and receives renewal notices via email The agent explains how to find the relevant email attachment and share it with the agent for processing The customer seeks advice on which policy to purchase and shares personal details such as height, weight, and income The agent informs the customer about waiting periods, critical and seasonal diseases, and exclusions The customer mentions a past operation and inquires about reimbursement, but the agent explains the limitations The agent collects information about the nominee, Kamini Devi, and the customer, Pardeep Kumar The agent arranges a call for OTP verification and advises the customer on the call-back process after filling out the form",
"question": "Which insurance policy does the agent advise",
"answer": "The agent advises the Aditya Birla Diamond plan",
},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "Who contacted the customer and inquired about their proposal form and payment?",
"answer": "Agent Vaibhav contacted the customer and inquired about their proposal form and payment"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "Was there any issue with the payment process according to the customer?",
"answer": "According to the customer, there were no problems with the payment process"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "What issue did Vaibhav inform the customer about?",
"answer": "Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customers pre-existing medical conditions"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "Is there a waiting period for pre-existing diseases mentioned in the proposal form, and if so, how long is it?",
"answer": "Yes, there is a waiting period of three years for pre-existing diseases mentioned in the proposal form"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "What information did Vaibhav provide about health check-ups?",
"answer": "Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panel"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "When will the soft copy of the policy be emailed to the customer after premium payment?",
"answer": "The soft copy of the policy will be emailed within 24 to 48 hours after premium payment"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "When can claims be made for all diseases under the policy?",
"answer": "Claims for all diseases can be made 30 days after policy issuance"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "When can claims be made for pre-existing diseases under the policy?",
"answer": "Claims for pre-existing diseases can be made after three years"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "What was the annual premium stated for the policy?",
"answer": "The annual premium for the policy was stated as 22,826 rupees"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "Did the customers wife initially decline the policy, and what changed her decision?",
"answer": "Yes, the customers wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "Are there any benefits for the customers 18-year-old son?",
"answer": "Yes, the son would receive all the policy benefits"},
{"context": "Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customerVaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries",
"question": "Who did Vaibhav mention might have contacted the customer previously, and what contact information did he provide?",
"answer": "Vaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queries"}
]


# Create a DataFrame from your qa_data
qa_df = pd.DataFrame(qa_data)

# Convert qa_df to a dataset
qa_dataset = Dataset.from_pandas(qa_df)

# Define the system prompt
DEFAULT_SYSTEM_PROMPT = """
Below is a conversation between an agent and a customer. Write the answer to the question based on the context.
""".strip()

# Define a function to generate training prompts
def generate_training_prompt(context, question, answer, system_prompt=DEFAULT_SYSTEM_PROMPT):
    return f"""### Instruction: {system_prompt}

### Context:
{context.strip()}

### Question:
{question.strip()}

### Answer:
{answer}
""".strip()

# Apply the generate_training_prompt function to your qa_dataset
qa_dataset = qa_dataset.map(
    lambda example: {
        "text": generate_training_prompt(example["context"], example["question"], example["answer"])
    },
    remove_columns=["context", "question", "answer"]
)

# Create a model and tokenizer
def create_model_and_tokenizer():
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=torch.float16,
    )

    model = AutoModelForCausalLM.from_pretrained(
         MODEL_NAME,
        # use_safetensors=True,
        quantization_config=bnb_config,
        trust_remote_code=True,
        device_map="auto",
    )

    tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)
    tokenizer.pad_token = tokenizer.eos_token
    tokenizer.padding_side = "right"

    return model, tokenizer

# Create the model and tokenizer
model, tokenizer = create_model_and_tokenizer()
model.config.use_cache = False

# Define LoRA parameters (if needed)
lora_r = 16
lora_alpha = 64
lora_dropout = 0.1
lora_target_modules = [
    "q_proj",
    "up_proj",
    "o_proj",
    "k_proj",
    "down_proj",
    "gate_proj",
    "v_proj",
]

# Define PeftConfig (if needed)
peft_config = LoraConfig(
    r=lora_r,
    lora_alpha=lora_alpha,
    lora_dropout=lora_dropout,
    target_modules=lora_target_modules,
    bias="none",
    task_type="CAUSAL_LM",
)
OUTPUT_DIR = "experiments"

# Define training arguments
training_arguments = TrainingArguments(
    per_device_train_batch_size=1,
    gradient_accumulation_steps=1,
    optim="paged_adamw_32bit",
    logging_steps=1,
    learning_rate=1e-4,
    fp16=True,
    max_grad_norm=0.3,
    num_train_epochs=10,
    evaluation_strategy="steps",
    eval_steps=0.2,
    warmup_ratio=0.05,
    save_strategy="epoch",
    group_by_length=True,
    output_dir=OUTPUT_DIR,
    report_to="tensorboard",
    save_safetensors=True,
    lr_scheduler_type="cosine",
    seed=42,
)

# Create a trainer
trainer = SFTTrainer(
    model=model,
    train_dataset=qa_dataset,
    eval_dataset=qa_dataset,
    peft_config=peft_config,
    dataset_text_field="text",
    max_seq_length=2096,
    tokenizer=tokenizer,
    args=training_arguments,
)

# Train the model on your qa_dataset
trainer.train()



def summarize(model, text: str):
    inputs = tokenizer(text, return_tensors="pt").to(DEVICE)
    inputs_length = len(inputs["input_ids"][0])
    with torch.inference_mode():
        outputs = model.generate(**inputs, max_new_tokens=256, temperature=0.0001)
    return tokenizer.decode(outputs[0][inputs_length:], skip_special_tokens=True)
# Example prompt
prompt = """
Below is a conversation between an agent and a customer. Write the answer of the one question based on the context.

### Context:
Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customer Vaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries


### Question:
when can the customer claim the policy?


### Answer:
"""

# Generate predictions using the provided prompt
predictions = summarize(model, prompt)

# Print the predictions
print("Predicted Answer:", predictions)
