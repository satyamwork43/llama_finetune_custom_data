# LLAMA_Finetune_Custom_Data

# Requirements

!pip install -Uqqq pip --progress-bar off

!pip install -qqq torch==2.0.1 --progress-bar off

!pip install -qqq transformers==4.32.1 --progress-bar off

!pip install -qqq datasets==2.14.4 --progress-bar off

!pip install -qqq peft==0.5.0 --progress-bar off

!pip install -qqq bitsandbytes==0.41.1 --progress-bar off

!pip install -qqq trl==0.7.1 --progress-bar off

#Training Process

Sure! Let's break down this code line by line:

### Import Libraries
```python
import json
import re
import pandas as pd
import torch
from datasets import Dataset
from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    BitsAndBytesConfig,
    TrainingArguments,
)
from trl import SFTTrainer
from peft import LoraConfig
```
- `import json`: Import the JSON library for working with JSON data.
- `import re`: Import the regular expressions library for string pattern matching.
- `import pandas as pd`: Import pandas library and alias it as `pd` for data manipulation and analysis.
- `import torch`: Import PyTorch for tensor computation and neural network operations.
- `from datasets import Dataset`: Import the `Dataset` class from the `datasets` library for handling datasets.
- `from transformers import (...)`: Import various classes and functions from the `transformers` library:
  - `AutoModelForCausalLM`: Automatically loads a pre-trained model for causal language modeling.
  - `AutoTokenizer`: Automatically loads a tokenizer for the model.
  - `BitsAndBytesConfig`: Configuration for using bits and bytes quantization.
  - `TrainingArguments`: Configuration for training arguments.
- `from trl import SFTTrainer`: Import the `SFTTrainer` class from the `trl` library for supervised fine-tuning.
- `from peft import LoraConfig`: Import `LoraConfig` for configuring LoRA (Low-Rank Adaptation) parameters.

### Device Setup
```python
DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"
MODEL_NAME = "TheBloke/Llama-2-7B-fp16"
```
- `DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"`: Set the computation device to GPU if available, otherwise use CPU.
- `MODEL_NAME = "TheBloke/Llama-2-7B-fp16"`: Set the model name to be used.

### Data Preparation
```python
# Define your qa_data

# Create a DataFrame from your qa_data
qa_df = pd.DataFrame(qa_data)

# Convert qa_df to a dataset
qa_dataset = Dataset.from_pandas(qa_df)
```
- `qa_data`: Placeholder for your question-answer data.
- `qa_df = pd.DataFrame(qa_data)`: Convert `qa_data` into a pandas DataFrame.
- `qa_dataset = Dataset.from_pandas(qa_df)`: Convert the DataFrame into a `Dataset` object.

### System Prompt
```python
# Define the system prompt
DEFAULT_SYSTEM_PROMPT = """
Below is a conversation between an agent and a customer. Write the answer to the question based on the context.
""".strip()
```
- Define the default system prompt for generating training prompts.

### Generate Training Prompts
```python
# Define a function to generate training prompts
def generate_training_prompt(context, question, answer, system_prompt=DEFAULT_SYSTEM_PROMPT):
    return f"""### Instruction: {system_prompt}

### Context:
{context.strip()}

### Question:
{question.strip()}

### Answer:
{answer}
""".strip()
```
- Define a function `generate_training_prompt` to format the context, question, and answer into a training prompt.

### Apply the Prompt Generation Function
```python
# Apply the generate_training_prompt function to your qa_dataset
qa_dataset = qa_dataset.map(
    lambda example: {
        "text": generate_training_prompt(example["context"], example["question"], example["answer"])
    },
    remove_columns=["context", "question", "answer"]
)
```
- Apply the `generate_training_prompt` function to each example in `qa_dataset` and keep only the generated text.

### Model and Tokenizer Creation
```python
# Create a model and tokenizer
def create_model_and_tokenizer():
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=torch.float16,
    )

    model = AutoModelForCausalLM.from_pretrained(
         MODEL_NAME,
        quantization_config=bnb_config,
        trust_remote_code=True,
        device_map="auto",
    )

    tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)
    tokenizer.pad_token = tokenizer.eos_token
    tokenizer.padding_side = "right"

    return model, tokenizer
```
- Define a function `create_model_and_tokenizer` to:
  - Create a `BitsAndBytesConfig` for quantization.
  - Load the model with the specified configuration.
  - Load the tokenizer and set padding tokens.

### Instantiate Model and Tokenizer
```python
# Create the model and tokenizer
model, tokenizer = create_model_and_tokenizer()
model.config.use_cache = False
```
- Create the model and tokenizer using the function defined earlier.
- Disable caching in the model configuration.

### LoRA Configuration
```python
# Define LoRA parameters (if needed)
lora_r = 16
lora_alpha = 64
lora_dropout = 0.1
lora_target_modules = [
    "q_proj",
    "up_proj",
    "o_proj",
    "k_proj",
    "down_proj",
    "gate_proj",
    "v_proj",
]

# Define PeftConfig (if needed)
peft_config = LoraConfig(
    r=lora_r,
    lora_alpha=lora_alpha,
    lora_dropout=lora_dropout,
    target_modules=lora_target_modules,
    bias="none",
    task_type="CAUSAL_LM",
)
```
- Define the parameters and configuration for LoRA (Low-Rank Adaptation).

### Training Arguments
```python
OUTPUT_DIR = "experiments"

# Define training arguments
training_arguments = TrainingArguments(
    per_device_train_batch_size=1,
    gradient_accumulation_steps=1,
    optim="paged_adamw_32bit",
    logging_steps=1,
    learning_rate=1e-4,
    fp16=True,
    max_grad_norm=0.3,
    num_train_epochs=10,
    evaluation_strategy="steps",
    eval_steps=0.2,
    warmup_ratio=0.05,
    save_strategy="epoch",
    group_by_length=True,
    output_dir=OUTPUT_DIR,
    report_to="tensorboard",
    save_safetensors=True,
    lr_scheduler_type="cosine",
    seed=42,
)
```
- Set the output directory for experiments.
- Define various training arguments, including batch size, learning rate, number of epochs, evaluation strategy, and more.

### Trainer Creation and Training
```python
# Create a trainer
trainer = SFTTrainer(
    model=model,
    train_dataset=qa_dataset,
    eval_dataset=qa_dataset,
    peft_config=peft_config,
    dataset_text_field="text",
    max_seq_length=2096,
    tokenizer=tokenizer,
    args=training_arguments,
)

# Train the model on your qa_dataset
trainer.train()
```
- Create an `SFTTrainer` instance with the model, datasets, tokenizer, and training arguments.
- Train the model using the `train` method of the trainer.

# Prediction code


def summarize(model, text: str):

    inputs = tokenizer(text, return_tensors="pt").to(DEVICE)

    inputs_length = len(inputs["input_ids"][0])

    with torch.inference_mode():

        outputs = model.generate(**inputs, max_new_tokens=256, temperature=0.0001)

    return tokenizer.decode(outputs[0][inputs_length:], skip_special_tokens=True)

# Example prompt

prompt = """
Below is a conversation between an agent and a customer. Write the answer of the one question based on the context.

### Context:

Agent Vaibhav contacted the customer and inquired about any issues with their proposal form and payment The customer stated that they had no problems with the payment process However, Vaibhav informed the customer that there was an issue with the proposal form link, which contained information about the customer pre-existing medical conditions, causing concern for the customer Vaibhav clarified that there is a waiting period of three years for pre-existing diseases mentioned in the proposal form The customer inquired about health check-ups, and Vaibhav explained that all members covered by the policy could undergo a health check-up once a year Additionally, policyholders could schedule a health check-up two days in advance at any nearest diagnostic lab within the panelVaibhav also mentioned that the soft copy of the policy would be emailed within 24 to 48 hours after premium payment He highlighted that claims for all diseases could be made 30 days after policy issuance, whereas claims for pre-existing diseases could be made after three years He encouraged the customer to expedite the policy processing as the link could expire after some timeThe customer wife initially declined the policy, but Vaibhav emphasized the importance of securing the policy to avoid future financial burdens, given the existing health issues The annual premium for the policy was stated as 22,826 rupeesVaibhav mentioned that another agent, Vivek, might have contacted the customer previously and provided his email address (vaibahvvermani@insurancedekhocom) for any future concerns or queriesThe customer also inquired about benefits for their 18-year-old son, and Vaibhav confirmed that the son would receive all the policy benefits In conclusion, Vaibhav informed the customer that he would transfer the call back to the previous agent, who would provide further information and address any remaining queries


### Question:

when can the customer claim the policy?


### Answer:

"""

# Generate predictions using the provided prompt

predictions = summarize(model, prompt)

# Print the predictions

print("Predicted Answer:", predictions)



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/satyamwork43/llama_finetune_custom_data.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/satyamwork43/llama_finetune_custom_data/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
